import toastr from 'toastr';
import { ValidationError } from 'yup';
import { authApi } from './api';
import { IAuthRequest, IValidationError } from './types';
import '../../../css/style.scss';

const authContainerEl = document.querySelector('#authContainer');

const showErrors = (errors: IValidationError[], formEl: HTMLFormElement) => {
    for (const validationError of errors) {
        const inputEl = formEl.querySelector(
            `input[name=${validationError.path}]`,
        ) as HTMLInputElement;
        if (inputEl) {
            const nextEl = inputEl.nextElementSibling as HTMLDivElement;
            const isExistHelpTextWrap = nextEl?.classList.contains('form__help-texts-wrap');
            const helpTextWrapEl = isExistHelpTextWrap ? nextEl : document.createElement('div');
            const helpTextEl = document.createElement('div');
            const [message] = validationError.errors;

            helpTextWrapEl.classList.add('form__help-texts-wrap');

            inputEl.style.borderColor = 'red';

            helpTextEl.style.fontSize = '12px';
            helpTextEl.style.color = 'red';
            helpTextEl.textContent = message;
            helpTextWrapEl.append(helpTextEl);

            if (!isExistHelpTextWrap) {
                inputEl.insertAdjacentElement('afterend', helpTextWrapEl);
            }
        }
    }
};

const clearInputsErrors = () => {
    const elems = authContainerEl?.querySelectorAll('.form__help-texts-wrap');
    if (elems) {
        for (const el of elems) {
            const inputEl = el.previousElementSibling as HTMLInputElement;
            inputEl.style.borderColor = 'green';
            el.innerHTML = '';
        }
    }
};

const handleFormSubmit = async (event: Event) => {
    event.preventDefault();

    const formEl = event.target as HTMLFormElement;
    const formData = new FormData(formEl) as unknown as Iterable<[IAuthRequest, string]>;
    const isBlockRegister = window.location.hash === '#register' || !window.location.hash;
    const isBlockLogin = window.location.hash === '#login';
    const data: IAuthRequest & { terms?: string } = Object.fromEntries(formData);

    clearInputsErrors();

    if (isBlockRegister && !data.terms) {
        toastr.error('you must agree with the rules of the bank', 'ERROR');

        return;
    }

    delete data.terms;

    try {
        if (isBlockRegister) {
            await authApi.register(data);
        }

        if (isBlockLogin) {
            const { email, password } = data;
            await authApi.login({ email, password });
        }
    } catch (error) {
        if (error instanceof ValidationError) {
            const reasons = error as ValidationError;
            const validationErrors = reasons.inner.map((reason: any) => ({
                type: reason.type,
                path: reason.path,
                errors: reason.errors,
            }));

            showErrors(validationErrors, formEl);
        }
    }
};

const hideAuthBlocks = () => {
    const loginEl = document.querySelector('#login') as HTMLDivElement;
    const registerEl = document.querySelector('#register') as HTMLDivElement;
    loginEl.style.display = 'none';
    registerEl.style.display = 'none';
};

const showAuthBlockById = (id: string) => {
    const authBlockEl = document.querySelector(id) as HTMLDivElement;
    if (authBlockEl) {
        authBlockEl.style.display = 'block';
    }
};

if (authContainerEl) {
    authContainerEl.addEventListener('click', (event) => {
        const target = event.target as HTMLAnchorElement;
        if ('linkToForm' in target.dataset) {
            event.preventDefault();
            hideAuthBlocks();
            showAuthBlockById(target.hash);

            window.history.pushState(
                { url: target.hash },
                document.title,
                location.pathname + target.hash,
            );
        }
        const forms = authContainerEl.querySelectorAll('form');
        for (const formEl of forms) {
            formEl.addEventListener('submit', handleFormSubmit);
        }
    });
}
