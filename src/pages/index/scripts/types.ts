export interface IAuthRequest {
    name: string;
    email: string;
    tel: string;
    password: string;
}

export interface IValidationError {
    type: string;
    path: string;
    errors: string[];
}

export interface IAuthResponse {
    data: string;
}
