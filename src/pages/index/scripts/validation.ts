import * as yup from 'yup';

export const authSchema = yup.object({
    name: yup
        .string()
        .required('name is a required field')
        .max(15, 'the maximum length must be 15 characters')
        .min(2, 'the minimum length must be 2 characters'),
    email: yup.string().email().required('email is a required field'),
    phone: yup
        .string()
        .required('phone is a required field')
        .max(13, 'the maximum length must be 13 characters')
        .min(10, 'the minimum length must be 10 characters'),
    password: yup.string().required('password is a required field').min(8),
});
