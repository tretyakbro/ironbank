import { axiosInstance } from '../../../shared/services/base-query';
import { IAuthRequest, IAuthResponse } from './types';
import { authSchema } from './validation';

export const authApi = Object.freeze({
    register: async (data: IAuthRequest) => {
        await authSchema.validate(data, { abortEarly: false });
        const { data: res } = await axiosInstance.post<IAuthResponse>('/register', data);
        localStorage.setItem('token', res.data);
        window.location.href = 'cards.html';
    },
    login: async (data: Pick<IAuthRequest, 'email' | 'password'>) => {
        await authSchema.pick(['email', 'password']).validate(data, { abortEarly: false });
        const { data: res } = await axiosInstance.post<IAuthResponse>('/login', data);
        localStorage.setItem('token', res.data);
        window.location.href = 'cards.html';
    },
});
