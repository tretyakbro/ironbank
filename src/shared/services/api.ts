import { axiosInstance } from './base-query';

export const mainApi = Object.freeze({
    auth: async (): Promise<void> => {
        await axiosInstance.get('/auth');
    },
});
