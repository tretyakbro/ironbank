import toastr from 'toastr';
import axios, { AxiosError, AxiosResponse } from 'axios';
import { IDefaultError } from './types';

const baseURL = 'https://lab.lectrum.io/js2/api/ironbank';

const token = localStorage.getItem('token');

export const axiosInstance = axios.create({
    baseURL,
    headers: {
        'x-token': token,
    },
});

const onResponseSuucess = (value: AxiosResponse): AxiosResponse => {
    return value;
};
const onError = (error: AxiosError): AxiosError | IDefaultError => {
    const defaultError = {
        message: 'Something went wrong',
    };

    const errorData: any = error.toJSON();
    if (error.response?.status === 401) {
        localStorage.removeItem('token');
        window.location.href = '/#login';
    }

    toastr.error(
        `${errorData?.config?.url}: ${errorData?.message}` || defaultError.message,
        'Error',
    );

    return errorData || defaultError;
};

axiosInstance.interceptors.response.use(onResponseSuucess, onError);
