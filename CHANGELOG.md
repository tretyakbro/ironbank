# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2022-12-12)


### Features

* **custom:** test ([8e2fe63](https://gitlab.com/tretyakbro/ironbank/commit/8e2fe6382949c8f032f1f3ef2d12065b79999cd6))
* **pages/index/api:** implement ([72094eb](https://gitlab.com/tretyakbro/ironbank/commit/72094eb27ba281e66c26c8695cd4c08abf7029a0))
* **pages/index/validation:** add auth shema ([a9a4fdc](https://gitlab.com/tretyakbro/ironbank/commit/a9a4fdc20340a5c953d823d492dc1a03549df523))
* **pages/index:** handling errors & forms & api ([3ef2a5d](https://gitlab.com/tretyakbro/ironbank/commit/3ef2a5da320f60630a0327dd01b8f996ec5627cf))
* **pages/index:** implement logic switching authentication blocks ([6973f16](https://gitlab.com/tretyakbro/ironbank/commit/6973f161279f4f19f9ac483310951a200aef1ca6))
* **shared/mainApi:** add auth & call it on page cards ([a044052](https://gitlab.com/tretyakbro/ironbank/commit/a044052b381b1e4c8f384f76f0e8cbce30f43aa8))
* **shared/services/base-query:** add error handlers ([8a78594](https://gitlab.com/tretyakbro/ironbank/commit/8a78594a92be6214b8f19b76da2e9923cc2a5ef4))
* **source:** initial commit ([3a0623b](https://gitlab.com/tretyakbro/ironbank/commit/3a0623b61d89c498f6c093c867b0644b3b419542))


### Bug Fixes

* **styles:** add checkbox error modifier & help-texts-wrap ([804b6d4](https://gitlab.com/tretyakbro/ironbank/commit/804b6d4a3132e494184e5565fc9d8893456da9af))
